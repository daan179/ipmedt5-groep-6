#include <Servo.h> 
 #include <Wire.h>
 #include <LiquidCrystal_I2C.h>


LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // LCD I2C address


 
 Servo mijnservo1;
 Servo mijnservo2;
 
int randNumberRobot;
int randNumberSpeler;
int lightPin1 = A0;  //Sensor 1
int lightPin2 = A1;  //Sensor 2
int STARTlightPinWaarde1;
int STARTlightPinWaarde2;
int lightPinWaarde1; //Waarde Sensor 1
int lightPinWaarde2; //Waarde Sensor 2
int trip1=0;
int trip2=0;
int zet=3;
int leeg  = 0;
int puntR = 0;
int puntS = 0;

const int  buttonPin = 7;    // De pin van de start knop



int buttonState;         // Huidige status van de knop
int lastButtonState;     // Vorige status van de knop




void setup(){
  Serial.begin(9600);
  pinMode(buttonPin, INPUT);       // Pins standard waarden
  digitalWrite(buttonPin, HIGH);   // turn on pullup resistors. Wire button so that press shorts pin to ground.
  STARTlightPinWaarde1 = analogRead(lightPin1);
  STARTlightPinWaarde2 = analogRead(lightPin2);
   
   lcd.begin(20,4);         // 20 chars met 4 lijnen starten, zet backlight aan
    
      for(int i = 0; i< 3; i++)
                                  {
                                    lcd.backlight();
                                    delay(250);
                                    lcd.noBacklight();
                                    delay(250);
                                  }
  
  lcd.backlight(); // backlight aan  

  
 mijnservo1.attach(9);  // Servo 1
  mijnservo2.attach(8);  // Servo 2

 randomSeed(analogRead(0));


 
 
}

void loop() {

 buttonState = digitalRead(buttonPin);                   // Leest de status van de button
 
 lcd.setCursor(0,1); //Start character 4 op lijn 0
                        lcd.print("DRUK OP START");
                         myservo1.write(0);
                          myservo2.write(0);
                                     
if (buttonState != lastButtonState) {
                        

  if (buttonState == HIGH){     // check of er een game gestart moet worden

      int speler = 0;
      int robot = 0;

     for (int ronde = 0; ronde <=3 ; ronde++ ) {         
            zet = 3;
                 myservo1.write(0);
                 myservo2.write(0);
                                                    
                                                       if (ronde == 3)
                                                        {
                                               
                                                             if (puntR < puntS)
                                                             {
                                                              lcd.setCursor(0,1);
                                                            lcd.print("YOU WIN!");
                                                             delay(3000);            // wacht 5 seconden 
                                                             lcd.clear();
                                                             }
                                                             else if  (puntR > puntS)
                                                             {
                                                              lcd.setCursor(0,1);
                                                            lcd.print("YOU LOST");
                                                             delay(3000);            // wacht 5 seconden 
                                                             lcd.clear();
                                                             }

                                                             else if  (puntR == puntS)
                                                             {
                                                              lcd.setCursor(0,1);
                                                            lcd.print("IT'S A TIE");
                                                             delay(3000);            // wacht 5 seconden 
                                                             lcd.clear();
                                                             }
                                              
                                                          lcd.setCursor(0,1);
                                                            lcd.print("END GAME");
                                                             delay(5000);            // wacht 5 seconden 
                                                             
                                                              lcd.clear();
                                                             return;
                                                        }
    
            lcd.clear();
        // willekeurig nummer tussen 0 en 2
    
      randNumberRobot = random(3);
      
      Serial.println(randNumberRobot);
      
                              lcd.setCursor(2,1); //Start at character 4 on line 0
                            lcd.print("Ronde ");
                            lcd.setCursor(8,1); //Start at character 4 on line 0
                            lcd.print(ronde + 1);
                            lcd.setCursor(10,1); //Start at character 4 on line 0
                            lcd.print("start in:");
                             delay(2000);
                             lcd.clear();


    buttonState = digitalRead(buttonPin);                   // Leest de status van de button

                            
                             lcd.setCursor(8,1); //Start at character 4 on line 0
                            lcd.print("3");
                            delay(1500);
                            lcd.clear();

                             lcd.setCursor(8,1); //Start at character 4 on line 0
                            lcd.print("2");
                            delay(1500);
                            lcd.clear();
                            
                             lcd.setCursor(8,1); //Start at character 4 on line 0
                            lcd.print("1");
                            delay(1500);
                            lcd.clear();

                            lcd.setCursor(8,1); //Start at character 4 on line 0
                            lcd.print("Doe uw zet!");
                          
                            SpelerZet();

                            if (SpelerZet() == 3)
                            {
                              lcd.setCursor(0,1);
                                                                 lcd.print("Spel is gereset");
                                                                                  
                                                                                  
                                                                 delay(5000);            // wacht 5 seconden 
                                                                 lcd.clear();
                                                                 return;
                            }
                            
                            int Szet = SpelerZet();
                            
                            
                             
                             
                                                             if ( randNumberRobot == 0  )       // schaar
                                                        {
                                                           myservo1.write(180);
                                                            
                                                            lcd.setCursor(2,1);
                                                            lcd.print("Robot:  Schaar");
                                                            
                                                            
                                                            delay(5000);                // wacht 5 seconden 

                                                                                                   
                                                            lcd.clear();
                                                            winnaar( randNumberRobot, Szet);
                                                            myservo1.write(0);
                                                            
                
                                                        }
                                                       else if ( randNumberRobot == 1 )            // steen
                                                        {
                                                           myservo1.write(180);
                                                           myservo2.write(180);
    
                                                            lcd.setCursor(2,1);
                                                            lcd.print("Robot:  Steen");
                
                                                               delay(5000);             // wacht 5 seconden 
                                                                lcd.clear();
                                                                winnaar( randNumberRobot, Szet);
                                                           myservo1.write(0);
                                                           myservo2.write(0);
                                                           
                                                   
                                                        }
                                                      else  if ( randNumberRobot == 2 )            // papier
                                                        {
                                                           lcd.setCursor(2,1);
                                                            lcd.print("Robot:  Papier");
                                                            
                                                            
                                                               delay(5000);            // wacht 5 seconden 
                                                             
                                                              lcd.clear();
                                                               winnaar( randNumberRobot, Szet);
                                                              
                                                        }

                                                       
                                                        
                                  }
    
                               }
                      
              }
              lastButtonState = buttonState;

}
void winnaar(int zetR, int zetS)
{
if (zetR == zetS){
   lcd.setCursor(2,1);
                                                                                                lcd.print("Gelijkspel");
                                                                                                
                                                                                                
                                                                                                   delay(5000);            // wacht 5 seconden 
                                                                                                  lcd.clear();


                    }
else if (zetS == 0)
{
          if (zetR == 2)
          {
               lcd.setCursor(2,1);
                                                                                                lcd.print("Speler wint"); 
                                                                                                   delay(5000);            // wacht 5 seconden 
                                                                                                  lcd.clear();
                                                                                                  puntS ++;
          }
          else if (zetR == 1)
          {
            lcd.setCursor(2,1);
                                                                                                lcd.print("Robot wint"); 
                                                                                                   delay(5000);            // wacht 5 seconden 
                                                                                                  lcd.clear();
                                                                                                  puntR ++;
          }
}
else if (zetS == 1)
{
          if (zetR == 0)
          {
               lcd.setCursor(2,1);
                                                                                                lcd.print("Speler wint"); 
                                                                                                   delay(5000);            // wacht 5 seconden 
                                                                                                  lcd.clear();
                                                                                                  puntS++;
          }
          else if (zetR == 2)
          {
            lcd.setCursor(2,1);
                                                                                                lcd.print("Robot wint"); 
                                                                                                   delay(5000);            // wacht 5 seconden 
                                                                                                  lcd.clear();
                                                                                                  puntR++;
          }
}
else if (zetS == 2)
{
          if (zetR == 1)
          {
               lcd.setCursor(2,1);
                                                                                                lcd.print("Speler wint"); 
                                                                                                   delay(5000);            // wacht 5 seconden 
                                                                                                  lcd.clear();
                                                                                                  puntS++;
          }
          else if (zetR == 0)
          {
            lcd.setCursor(2,1);
                                                                                                lcd.print("Robot wint"); 
                                                                                                   delay(5000);            // wacht 5 seconden 
                                                                                                  lcd.clear();
                                                                                                  puntR++;
          }
}


 
}

int SpelerZet()
{
trip1=0;
trip2=0;
  while (zet == 3)
                  {
                    updateLight();
                                    if (lightPinWaarde1 < STARTlightPinWaarde1 - 100 && leeg == 0)
                                    {
                                    trip1 ++;
                                    leeg = 1;
                                    
                                    }
                                       if (leeg== 1 && lightPinWaarde1 > STARTlightPinWaarde1 - 100)
                                    {
                                      
                                    
                                    leeg = 0;
                                    
                                    
                                    }
                                    
                    updateLight();
                                    if (trip1 == 2 && trip2 ==0)
                                    {
                                                                                                lcd.setCursor(2,1);
                                                                                                lcd.print("Speler zet schaar"); 
                                                                                                   delay(3000);            // wacht 3 seconden 
                                                                                                    lcd.clear();
                                    //Serial.println("Schaar");
                                    zet = 0;
                                    
                                    } 
                    updateLight();
                                    if (lightPinWaarde2 < STARTlightPinWaarde2 - 100)
                                    {
                                    trip2 ++;
                                 
                                    }  
                    updateLight();
                                    if (lightPinWaarde1 > STARTlightPinWaarde1 - 100 && trip2 == 1)
                                    {
                                  
                                                                                                lcd.setCursor(2,1);
                                                                                                lcd.print("Speler zet papier"); 
                                                                                                   delay(3000);            // wacht 3 seconden 
                                                                                                    lcd.clear();

                                    zet = 2;
                                    }

                    updateLight();
                                    if (lightPinWaarde1 < STARTlightPinWaarde1 - 100 && lightPinWaarde2 < STARTlightPinWaarde2 - 100 )
                                    {
                                    
                                                                                                lcd.setCursor(2,1);
                                                                                                lcd.print("Speler zet Steen"); 
                                                                                                   delay(3000);            // wacht 3 seconden 
                                                                                                    lcd.clear();
             
                                    zet = 1;
                                    
                                    }

                                     if (buttonState == LOW)
                                {
                                  buttonState = digitalRead(buttonPin);                   // Leest de status van de button
                                }

                                   if (buttonState == HIGH)
                                                      {

                                                                  trip1=0;
                                                                  trip2=0;
                                                                   leeg = 0;
                                                                  break;
                          
                                                       }

}

trip1=0;
trip2=0;
leeg = 0;
return zet;
}
void updateLight()
{
  lightPinWaarde1 = analogRead(lightPin1);
  lightPinWaarde2 = analogRead(lightPin2);
}


