//Wanneer de waarde van een lichtsensor onder een bepaald aantal
//valt, is hij geblokkkeerd. Aan de hand van hoe en hoe lang de

#include <TimerOne.h>
#include <avr/interrupt.h>

//sensoren worden geblokkeerd, wordt bepaald welke zet is gedaan
int lightPin1 = A0;  //Sensor 1
int lightPin2 = A1;  //Sensor 2


int lightPinWaarde1; //Waarde Sensor 1 
int lightPinWaarde2; //Waarde Sensor 2
int STARTlightPinWaarde1;
int STARTlightPinWaarde2;
int zetUitkomst;     //Welke zet is gedaan
int zetWaarde = 0;   //Bereken welke zet is gedaan
int sensCount1= 0;
int sensCount2= 0;

void setup() 
{
  Serial.begin(9600); //Begin het lezen van de sensor (voor de console)
  
  pinMode(10, OUTPUT);
  STARTlightPinWaarde1 = analogRead(lightPin1);
  STARTlightPinWaarde2 = analogRead(lightPin2); 

  Timer1.initialize(100000);
  Timer1.attachInterrupt(updateLight); // Deze funtie wordt elke 6 seconden uitgevoerd (maar na 5 seconden worden interrupts uitgezet, dus het gebeurt maar 1x) 
  //noInterrupts();
}

void loop(){
//Zorg er ten alle tijde voor dat lightPinWaarde gelijk is aan
//de waarde van de pin   

lightPinWaarde1 = analogRead(lightPin1);
lightPinWaarde2 = analogRead(lightPin2); 
Serial.println(lightPinWaarde1);
delay(250);
Serial.println(lightPinWaarde2);
delay(250);

while (lightPinWaarde1 > STARTlightPinWaarde1 - 100 ^ lightPinWaarde2 < STARTlightPinWaarde2 - 100 ^ sensCount2 != 2)
  {  
    if (lightPinWaarde1 < STARTlightPinWaarde1 - 100)
    {
      telZet();
      sensCount2 ++;
      Serial.println("Sensor 1");
      //delay(500);
      bepaalZet();
      
    }
   }

while (lightPinWaarde2 > STARTlightPinWaarde2 - 100 ^ lightPinWaarde1 < STARTlightPinWaarde1 - 100 ^ sensCount1 != 2)
  {  
    if (lightPinWaarde2 < STARTlightPinWaarde2 - 100)
    {
      telZet();
      sensCount1 ++;
      Serial.println("Sensor 2");
     // delay(500);
      bepaalZet();
      
    }
   }
//sensCount2 = 0;
   }   


//Een Timer Interrupt die elke 1/10 seconden handelt afhankelijk van de waarde lightPinWaarde1
void telZet(){

if (lightPinWaarde1 < STARTlightPinWaarde1 - 100 ^ lightPinWaarde2 < STARTlightPinWaarde2 - 100 )
  {
    zetWaarde = zetWaarde + 100;
  }
if (lightPinWaarde1 < STARTlightPinWaarde1 - 100 && lightPinWaarde2 < STARTlightPinWaarde2 - 100 )
  updateLight();
  {
    zetWaarde = 300;
    bepaalZet();
  }
}

//Update lightPinWaarde constant: dit is niet mogelijk in de loop vanwege de delay
void updateLight()
{
  lightPinWaarde1 = analogRead(lightPin1);
  lightPinWaarde2 = analogRead(lightPin2); 
 // Serial.println("Waarde 1");
  //Serial.println(lightPinWaarde1); 
  //Serial.println("Waarde 2");
 // Serial.println(lightPinWaarde2);
}

//Wijst een zet aan afhankelijk van de output van telZet
void bepaalZet()
{
  Serial.println("De waarde van de zet was");
  Serial.println(zetWaarde);

if(zetWaarde == 0)
  {
    Serial.println("U heeft geen zet gedaan!");
    Serial.println(zetWaarde);
  }
    
if(zetWaarde == 200)
  {
    zetUitkomst = 0;  //Papier
    digitalWrite(10, HIGH); 
    Serial.println("Papier!");
    Serial.println(zetWaarde);
    zetWaarde = 0;
  }

else if(zetWaarde == 300)
  {
    zetUitkomst = 1;  //Steen
    Serial.println("Steen!");
    Serial.println(zetWaarde);
    zetWaarde = 0;
  }

else if(zetWaarde == 400)
  {
    zetUitkomst = 2;  //Schaar
   Serial.println("Schaar!");
   Serial.println(zetWaarde);
    zetWaarde = 0;
  }

else if(zetWaarde < 401)
  {
    Serial.println("Ongeldige zet!");
    Serial.println(zetWaarde);
    zetWaarde = 0;
  }  
} 


