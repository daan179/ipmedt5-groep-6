//Wanneer de waarde van een lichtsensor onder een bepaald aantal
//valt, is hij geblokkkeerd. Aan de hand van hoe en hoe lang de

#include <TimerOne.h>
#include <avr/interrupt.h>

//sensoren worden geblokkeerd, wordt bepaald welke zet is gedaan
int lightPin1 = A0;  //Sensor 1
int lightPin2 = A1;  //Sensor 2


int lightPinWaarde1; //Waarde Sensor 1 
int lightPinWaarde2; //Waarde Sensor 2
int STARTlightPinWaarde1;
int STARTlightPinWaarde2;
int zetUitkomst;     //Welke zet is gedaan
int zetWaarde = 0;   //Bereken welke zet is gedaan

void setup() 
{
  Serial.begin(9600); //Begin het lezen van de sensor (voor de console)
  
  pinMode(10, OUTPUT);
  STARTlightPinWaarde1 = analogRead(lightPin1);
  STARTlightPinWaarde2 = analogRead(lightPin2); 

  Timer1.initialize(100000);
  Timer1.attachInterrupt(telZet); // Deze funtie wordt elke 6 seconden uitgevoerd (maar na 5 seconden worden interrupts uitgezet, dus het gebeurt maar 1x) 
  noInterrupts();
}

void loop(){
//Zorg er ten alle tijde voor dat lightPinWaarde gelijk is aan
//de waarde van de pin   

lightPinWaarde1 = analogRead(lightPin1);
lightPinWaarde2 = analogRead(lightPin2); 

if(lightPinWaarde1 < STARTlightPinWaarde1 - 100 || lightPinWaarde2 < STARTlightPinWaarde2 - 100)
  {  
    
    interrupts();
    Timer1.start(); 
    updateLight();
    delay(3000);
    Timer1.stop();
    bepaalZet();
    noInterrupts();
    
  }
} 
void telZet(){

  if(lightPinWaarde1 < 300 ^ lightPinWaarde2 < 300) //Waarde aanpassen op werkelijke lichtsterkte
  {
    //Ervoor zorgen dat dit slechts één keer gebeurt: alleen wanneer het verandert
    Serial.println("+100 check");
    zetWaarde = zetWaarde + 100;  //Als éen wordt geblokkeerd wordt opgeteld om te bepalen papier of schaar 
    delay(100);
  }                    

if(lightPinWaarde1 < 300 && lightPinWaarde2 < 300) //Zie boven
  {
    Serial.println("+300 check");
    zetWaarde = 300;  //Als beide zijn geblokkeerd is het steen
    delay(5000);
  } 

  //Serial.println(zetWaarde);
  updateLight();
  //Serial.println(lightPinWaarde1);
  //Serial.println(lightPinWaarde2);
}


void updateLight()
{
  lightPinWaarde1 = analogRead(lightPin1);
  lightPinWaarde2 = analogRead(lightPin2); 
}


void bepaalZet()
{
  Serial.println("De waarde van de zet was");
  Serial.println(zetWaarde);

if(zetWaarde == 0)
  {
    Serial.println("U heeft geen zet gedaan!");
    Serial.println(zetWaarde);
  }
    
if(zetWaarde == 200)
  {
    zetUitkomst = 0;  //Papier
    digitalWrite(10, HIGH); 
    Serial.println("Papier!");
    Serial.println(zetWaarde);
    zetWaarde = 0;
  }

else if(zetWaarde == 300)
  {
    zetUitkomst = 1;  //Steen
    Serial.println("Steen!");
    Serial.println(zetWaarde);
    zetWaarde = 0;
  }

else if(zetWaarde == 400)
  {
    zetUitkomst = 2;  //Schaar
   Serial.println("Schaar!");
   Serial.println(zetWaarde);
    zetWaarde = 0;
  }

else if(zetWaarde < 401)
  {
    Serial.println("Ongeldige zet!");
    Serial.println(zetWaarde);
  }  
} 


